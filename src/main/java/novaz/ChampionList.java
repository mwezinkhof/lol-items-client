package novaz;

import novaz.tools.Http;
import novaz.tools.Log;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by kaaz on 4-6-2015.
 */
public class ChampionList {
	static final String baseUrl = "http://champion.gg";
	static Queue<BuildCombo> queue = new LinkedList<BuildCombo>();
	static String patch = "NOT FOUND";
	static ArrayList<ItemSetData> output = new ArrayList<>();

	public static void GetChampionList() {
		InputStream in = null;
		String rawData = "";
		try {

			Document doc = Jsoup.parse(Http.request(baseUrl));
			patch = doc.select("div.analysis-holder strong").first().html();
			Elements els = doc.select("div.champ-height");
			for (Element e : els) {
				Elements aTag = e.select("a");
//				Log.fatal(aTag.size());
				String champion = aTag.get(0).select("span").html();
				for (int i = 1; i < aTag.size(); i++) {
					String role = aTag.get(i).html();
					String roleUrl = aTag.get(i).attr("href");
//					System.out.println(String.format("%12s %10s %s",champion,role,roleUrl));
					queue.add(new BuildCombo(champion, role, roleUrl));

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean processQueueItem() {
		BuildCombo queueItem = queue.poll();
		String[] abilities = new String[19];
		if (queueItem != null) {
			Log.info(String.format("Queue - %s@%s", queueItem.champion, queueItem.role));
			ItemSetData setFile = new ItemSetData(queueItem.champion, queueItem.role, patch);
			String html = Http.request(baseUrl + queueItem.url);
			Document doc = Jsoup.parse(html);
			Elements skillUps = doc.select("div.skill-selections");
			int skilbarIndex = 0;
			for (Element s : skillUps) {
				if (skilbarIndex++ > 4) {
					break;
				}
				Elements levels = s.select("div");
				for (int level = 0; level < levels.size(); level++) {
					if (levels.get(level).hasClass("selected")) {
						if (level < 19) {
							abilities[level] = levels.get(level).select("span").html();
						}
					}
				}
			}
			Elements groups = doc.select("div.build-wrapper");
			if (groups.size() == 4) {  //0,1,2,3 = Most Frequent Core Build, Highest Win % Core Build, Most Frequent Starters, Highest Win % Starters
				Element e = groups.get(1);//highest win full build
				Elements items = e.select("a img");
				for (Element item : items) {
					String rawItem = item.attr("src");
					int last = rawItem.lastIndexOf("/") + 1;
					setFile.addFinalItem(rawItem.substring(last, rawItem.length() - 4));
				}
				Element e2 = groups.get(3);//highest win starters
				Elements items2 = e2.select("a img");
				for (Element item : items2) {
					String rawItem = item.attr("src");
					int last = rawItem.lastIndexOf("/") + 1;
					setFile.addStartItem(rawItem.substring(last, rawItem.length() - 4));
				}
			}
			System.out.println(setFile.getFileName());
			File f = new File(Config.gameLocation + "/" + setFile.getFileName());
			f.getParentFile().mkdirs();
			try {
				FileUtils.writeStringToFile(f, setFile.toJson());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
}
