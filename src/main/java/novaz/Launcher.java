/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz;

import novaz.ui.OptionsPanel;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;

public class Launcher {
	public static void main(String... args) throws Exception {
		Config.init();
		Config.saveProperties();
		ChampionList.GetChampionList();
		int i=0;
		FileUtils.cleanDirectory(new File(Config.gameLocation));
		while(ChampionList.processQueueItem()){
//			if(i++ > 2){
			break;
//			}
		}
//		SwingUtilities.invokeLater(new Runnable() {
//			@Override
//			public void run() {
//				new OptionsPanel(null).setVisible(true);
//			}
//		});
	}
}
