package novaz;

import com.google.gson.Gson;
import novaz.json.Block;
import novaz.json.ChampionBuild;
import novaz.json.Item;
import novaz.tools.Log;

import java.util.ArrayList;

/**
 * Created by kaaz on 4-6-2015.
 */
public class ItemSetData {
	String map = "any";
	ArrayList<Item> consumables;
	ArrayList<Item> startSet;
	ArrayList<Item> finalSet;
	private String champion;
	private String role;
	private String patch;
	private String[] skillorder;

	public ItemSetData(String champion, String role, String patch) {
		this.champion = champion;
		this.role = role;
		this.patch = patch;
		startSet = new ArrayList<>();
		finalSet = new ArrayList<>();
		skillorder = new String[19];
	}
	public void setSkillOrder(String[] skillorder){
		this.skillorder = skillorder;
	}
	private String getSkillPriority(){
		String ret = "";
		int[] caps = {5,5,5,3};
		for(String skill : skillorder){
			skill = "";
		}
		return ret;
	}
	public void addStartItem(String itemid) {
		Item i = new Item();
		i.setId(itemid);
		i.setCount(1);
		startSet.add(i);
		Log.info("start:", itemid);
	}

	public void addFinalItem(String itemid) {
		Item i = new Item();
		i.setId(itemid);
		i.setCount(1);
		finalSet.add(i);
		Log.info("final:", itemid);
	}
	public String getFileName(){
		return champion + "/Recommended/"+patch+"_"+role+".json";
	}
	public String toJson() {
		ChampionBuild b = new ChampionBuild();
		b.setMap(map);
		b.setIsGlobalForChampions(false);
		ArrayList<Block> blocks = new ArrayList<>();
		Block startBlock = new Block();
		startBlock.setType("Starting Items");
		startBlock.setItems(startSet);
		Block finalBlock = new Block();
		finalBlock.setType("Full Build");
		finalBlock.setItems(finalSet);
		blocks.add(startBlock);
		blocks.add(finalBlock);
		b.setBlocks(blocks);

		b.setTitle(role + " " + patch);
		b.setPriority(false);
		b.setMode("any");
		b.setIsGlobalForMaps(true);
		b.setType("custom");
		b.setSortrank(1);
		b.setChampion(champion);
		return new Gson().toJson(b);
	}
}
