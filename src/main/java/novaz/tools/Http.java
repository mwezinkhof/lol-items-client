package novaz.tools;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by kaaz on 4-6-2015.
 */
public class Http {
	public static String request(String targetUrl){
		String rawData = "";
		try {
			URL url = new URL(targetUrl);
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/600.3.18 (KHTML, like Gecko) Version/8.0.3 Safari/600.3.18");
			conn.connect();
			BufferedReader serverResponse = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = serverResponse.readLine()) != null) {
				rawData += line;
			}

			serverResponse.close();
		}
		catch(Exception e){
			Log.fatal(e);
		}
		return rawData;
	}
}
