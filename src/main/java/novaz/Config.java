/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package novaz;

import novaz.tools.Log;
import novaz.tools.LogLevel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class Config {
	public static LogLevel loglevel = LogLevel.DEBUG;
	public static String gameLocation = "";

	private static String PROPERTY_FILE = "lolitems.cfg";

	public static void init() {
		loadProperties();
	}

	public static void loadProperties() {

		Properties p = getProps(PROPERTY_FILE);
		gameLocation = p.getProperty("game_location", "C:\\Riot Games\\League of Legends\\Config\\Champions");
	}

	public static void saveProperties() {
		Properties p = getProps(PROPERTY_FILE);
		p.put("game_location", gameLocation);

		try {
			p.store(new FileOutputStream(PROPERTY_FILE), null);
		} catch (Exception e) {
			Log.fatal("couldn't save property file");
			Log.fatal(e);
		}
	}

	private static Properties getProps(String filename) {
		try {
			File f = new File(filename);
			if (!f.exists()) {
				f.createNewFile();
			}
			Properties p = new Properties();
			p.load(new FileInputStream(f));
			return p;
		} catch (Exception e) {
			Log.fatal(e);
		}
		return new Properties();
	}
}
