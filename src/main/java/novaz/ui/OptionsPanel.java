/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package novaz.ui;

import novaz.ChampionList;
import novaz.Config;
import novaz.tools.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class OptionsPanel extends JDialog
		implements ActionListener,
		PropertyChangeListener {

	private List<Image> desktopIcons = Arrays.asList(
			new ImageIcon(OptionsPanel.class.getResource("/icon_16.png")).getImage(),
			new ImageIcon(OptionsPanel.class.getResource("/icon_32.png")).getImage(),
			new ImageIcon(OptionsPanel.class.getResource("/icon_64.png")).getImage());
	private JOptionPane optionPane;
	private String buttonLaunch = "Launch";
	private String buttonOpenExplorer = "Open directory";
	private String buttonNewPack = "New pack";
	private File launchFile;

	public OptionsPanel(Frame aFrame) {
		super(null, java.awt.Dialog.ModalityType.TOOLKIT_MODAL);
		setIconImages(desktopIcons);
		this.launchFile = launchFile;
		setTitle("LoL Item sets");

		String headerLabel = "Update the Recommended items";

		Object[] options = {buttonLaunch, buttonOpenExplorer, buttonNewPack};

		//Create the JOptionPane.
		Object[] array = {headerLabel};
		optionPane = new JOptionPane(array,
				JOptionPane.PLAIN_MESSAGE,
				JOptionPane.YES_NO_OPTION,
				null,
				options, null);
		setContentPane(optionPane);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		optionPane.addPropertyChangeListener(this);
		optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
	}

	public String[] getInstances(String packLocation) {
		File f = new File(packLocation);
		return f.list();
	}

	/**
	 * This method handles events for the text field.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	}

	/**
	 * This method reacts to state changes in the option pane.
	 */
	@Override
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();

		if (isVisible()
				&& (e.getSource() == optionPane)
				&& (JOptionPane.VALUE_PROPERTY.equals(prop)
				|| JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
			Object value = optionPane.getValue();
			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				//ignore reset
				return;
			}
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (buttonLaunch.equals(value)) {

				try {
					Log.debug("LAUNCH BUTTON");
//					ChampionList.GetChampionList();
				} catch (Exception error) {
					Log.fatal(error);
				}

			} else if (buttonOpenExplorer.equals(value)) {
				try {
					Desktop.getDesktop().open(new File(Config.gameLocation));
				} catch (IOException e1) {
					Log.fatal(e);
				}

			} else if (buttonNewPack.equals(value)) {
				String packName = JOptionPane.showInputDialog(this, "Name of the new pack:");
				String errors = "";
				if (packName != null) {

					JOptionPane.showMessageDialog(this,
							"Couldn't add pack: "
									+ errors, "Error",
							JOptionPane.ERROR_MESSAGE);

				}
			} else {
				exit();
			}
		}
	}

	public void exit() {
		dispose();
	}
}