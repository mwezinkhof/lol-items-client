package novaz.json;

/**
 * Created by kaaz on 4-6-2015.
 */

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class ChampionBuild {

	@Expose
	private String map;
	@Expose
	private Boolean isGlobalForChampions;
	@Expose
	private List<Block> blocks = new ArrayList<Block>();
	@Expose
	private List<Object> associatedChampions = new ArrayList<Object>();
	@Expose
	private String title;
	@Expose
	private Boolean priority;
	@Expose
	private String mode;
	@Expose
	private Boolean isGlobalForMaps;
	@Expose
	private List<Object> associatedMaps = new ArrayList<Object>();
	@Expose
	private String type;
	@Expose
	private Integer sortrank;
	@Expose
	private String champion;

	/**
	 *
	 * @return
	 * The map
	 */
	public String getMap() {
		return map;
	}

	/**
	 *
	 * @param map
	 * The map
	 */
	public void setMap(String map) {
		this.map = map;
	}

	/**
	 *
	 * @return
	 * The isGlobalForChampions
	 */
	public Boolean getIsGlobalForChampions() {
		return isGlobalForChampions;
	}

	/**
	 *
	 * @param isGlobalForChampions
	 * The isGlobalForChampions
	 */
	public void setIsGlobalForChampions(Boolean isGlobalForChampions) {
		this.isGlobalForChampions = isGlobalForChampions;
	}

	/**
	 *
	 * @return
	 * The blocks
	 */
	public List<Block> getBlocks() {
		return blocks;
	}

	/**
	 *
	 * @param blocks
	 * The blocks
	 */
	public void setBlocks(List<Block> blocks) {
		this.blocks = blocks;
	}

	/**
	 *
	 * @return
	 * The associatedChampions
	 */
	public List<Object> getAssociatedChampions() {
		return associatedChampions;
	}

	/**
	 *
	 * @param associatedChampions
	 * The associatedChampions
	 */
	public void setAssociatedChampions(List<Object> associatedChampions) {
		this.associatedChampions = associatedChampions;
	}

	/**
	 *
	 * @return
	 * The title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 *
	 * @param title
	 * The title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 *
	 * @return
	 * The priority
	 */
	public Boolean getPriority() {
		return priority;
	}

	/**
	 *
	 * @param priority
	 * The priority
	 */
	public void setPriority(Boolean priority) {
		this.priority = priority;
	}

	/**
	 *
	 * @return
	 * The mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 *
	 * @param mode
	 * The mode
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 *
	 * @return
	 * The isGlobalForMaps
	 */
	public Boolean getIsGlobalForMaps() {
		return isGlobalForMaps;
	}

	/**
	 *
	 * @param isGlobalForMaps
	 * The isGlobalForMaps
	 */
	public void setIsGlobalForMaps(Boolean isGlobalForMaps) {
		this.isGlobalForMaps = isGlobalForMaps;
	}

	/**
	 *
	 * @return
	 * The associatedMaps
	 */
	public List<Object> getAssociatedMaps() {
		return associatedMaps;
	}

	/**
	 *
	 * @param associatedMaps
	 * The associatedMaps
	 */
	public void setAssociatedMaps(List<Object> associatedMaps) {
		this.associatedMaps = associatedMaps;
	}

	/**
	 *
	 * @return
	 * The type
	 */
	public String getType() {
		return type;
	}

	/**
	 *
	 * @param type
	 * The type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 *
	 * @return
	 * The sortrank
	 */
	public Integer getSortrank() {
		return sortrank;
	}

	/**
	 *
	 * @param sortrank
	 * The sortrank
	 */
	public void setSortrank(Integer sortrank) {
		this.sortrank = sortrank;
	}

	/**
	 *
	 * @return
	 * The champion
	 */
	public String getChampion() {
		return champion;
	}

	/**
	 *
	 * @param champion
	 * The champion
	 */
	public void setChampion(String champion) {
		this.champion = champion;
	}

}