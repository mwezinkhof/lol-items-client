package novaz.json;

/**
 * Created by kaaz on 4-6-2015.
 */
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

public class Block {

	@Expose
	private List<Item> items = new ArrayList<Item>();
	@Expose
	private String type;

	/**
	 *
	 * @return
	 * The items
	 */
	public List<Item> getItems() {
		return items;
	}

	/**
	 *
	 * @param items
	 * The items
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}

	/**
	 *
	 * @return
	 * The type
	 */
	public String getType() {
		return type;
	}

	/**
	 *
	 * @param type
	 * The type
	 */
	public void setType(String type) {
		this.type = type;
	}

}
