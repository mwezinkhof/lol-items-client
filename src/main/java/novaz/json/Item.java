package novaz.json;

/**
 * Created by kaaz on 4-6-2015.
 */

import com.google.gson.annotations.Expose;

public class Item {

	@Expose
	private Integer count;
	@Expose
	private String id;

	/**
	 *
	 * @return
	 * The count
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 *
	 * @param count
	 * The count
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 *
	 * @return
	 * The id
	 */
	public String getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 * The id
	 */
	public void setId(String id) {
		this.id = id;
	}

}
